.global main

// x27 - Operande
// x28 - Opcode

.macro SAVE
	stp x29, x30, [sp, -96]!
	mov x29, sp
	stp x27, x28, [sp, 16]
	stp x25, x26, [sp, 32]
	stp x23, x24, [sp, 48]
	stp x21, x22, [sp, 64]
	stp x19, x20, [sp, 80]
.endm

.macro RESTORE
	ldp x27, x28, [sp, 16]
	ldp x25, x26, [sp, 32]
	ldp x23, x24, [sp, 48]
	ldp x21, x22, [sp, 64]
	ldp x19, x20, [sp, 80]
	ldp x29, x30, [sp], 96
.endm

MEMSIZE = 128

main:							//
	adr		x0, acc				//
	bl		showAcc				//	showAcc(*acc)
								//
	adr		x0, mem0			//
	mov		x1, 0				//
	bl		showMem				//	showMem(*mem[0], 0)
								//
	adr		x0, mem1			//
	mov		x1, 1				//
	bl		showMem				//	showMem(*mem[1], 1)
								//
	adr		x0, mem2			//
	mov		x1, 2				//
	bl		showMem				//	showMem(*mem[2], 2)
								//
	// Input Operande			//
    adr		x0, fmtOperande		//
	adr		x1,	nombre			//
    bl      scanf				//	scanf(*fmtOperande, *nombre)
    ldr   	x27, nombre			//	operande = &nombre
	cmp     x27, 5				//
    b.lo    main100				//	if(operande < 5) main100
	b		end					//	end
								//
main100:						//
								//
	// Input Opcode				//
    adr		x0, fmtOpcode		//
	adr		x1,	nombre			//
    bl      scanf               // 	scanf(&fmtOpcode, *nombre)
    ldr   	x28, nombre			//	opcode = &nombre
								//
    cmp     x27, 0				//
    b.eq    setAcc				//	if(operande == 0) setAcc
    cmp     x27, 1				//
    b.eq    setMemByAcc			//	if(operande == 1) setMemByAcc
    cmp     x27, 2				//
    b.eq    setAccByMem			//	if(operande == 2) setAccByMem
    cmp     x27, 3				//
    b.eq    memAddInit			//	if(operande == 3) memAddInit
    cmp     x27, 4				//
    b.eq    memSubInit			//	if(operande == 4) memSubInit


// Sous-programme: showAcc
// Entrées:
//   - adresse a
// Effet: —	Affiche un entier signé de 128 bits en hexadécimal
// Sortie: Aucune
showAcc:						//
	SAVE						//
	ldr		x19, [x0]			//	part1 = &a
	add		x0,	x0, 64			//	a += 64
	ldr		x20, [x0]			//	part2 = &a
	adr		x0, fmtAcc			//
	mov		x1, x19				//
	mov		x2, x20				//
	bl		printf				//	printf(*fmtAcc, part1, part2)
	RESTORE						//
	ret							//

// Sous-programme: showMem
// Entrées:
//   - adresse a
//	 - n
// Effet: —	Affiche un entier signé de 128 bits en hexadécimal
// Sortie: Aucune
showMem:						//
	SAVE						//
	ldr		x19, [x0]			//	part1 = &a
	add		x0,	x0, 64			//	*a += 64
	ldr		x20, [x0]			//	part2 = &a
	adr		x0, fmtMem			//
	mov		x2, x19				//
	mov		x3, x20				//
	bl		printf				//	printf(*fmtAcc, n, part1, part2)
	RESTORE						//
	ret							//

setAcc:							//
	adr		x19, acc			//	acc = *acc
	add		x19, x19, 64		//	acc += 64
	strb	w28, [x19]			//	&acc = opcode
	b		main				//	main

setMemByAcc:					//
	adr		x0, acc				// 	acc = *acc
	adr		x1, mem0			// 	mem[n] = *mem[0]
	cmp		x28, 0				//
	b.eq	setMemByAcc100		//	if(opcode == 0) setMemByAcc100
	adr		x1, mem1			//	mem[n] = *mem[1]
	cmp		x28, 1				//
	b.eq	setMemByAcc100		//	if(opcode == 1) setMemByAcc100
	adr		x1, mem2			//	mem[n] = *mem[2]
	cmp		x28, 2				//
	b.eq	setMemByAcc100		//	if(opcode == 2) setMemByAcc100

setMemByAcc100:					//
	bl		setMemByAcc50		//	setMemByAcc50(*acc, mem[n])
	b		main				//	main

// Sous-programme: setAccByMem50
// Entrées:
//   - adresse a
//	 - adresse b
// Effet: —	Store un entier signé de 128 bits a dans b
// Sortie: Aucune
setMemByAcc50:					//	void setMemByAcc50 {
	ldr		x19, [x0]			//		bytes = &a
	str		x19, [x1]			//		&b = bytes
	add		x0, x0, 64			//		*a += 64
	add		x1, x1, 64			//		*b += 64
	ldr		x19, [x0]			//		bytes = &a
	str		x19, [x1]			//		&b = bytes
	ret							//	}

setAccByMem:					//
	adr		x0, acc				// 	acc = *acc
	cmp		x28, 0				// 	mem[n] = *mem[0]
	adr		x1, mem0			//
	b.eq	setAccByMem100		//	if(opcode == 0) setAccByMem100
	cmp		x28, 1				//	mem[n] = *mem[1]
	adr		x1, mem1			//
	b.eq	setAccByMem100		//	if(opcode == 1) setAccByMem100
	cmp		x28, 2				//	mem[n] = *mem[2]
	adr		x1, mem2			//
	b.eq	setAccByMem100		//	if(opcode == 2) setAccByMem100

setAccByMem100:					//
	bl		setAccByMem50		//	setAccByMem50(*acc, mem[n])
	b		main				//	main

// Sous-programme: setAccByMem50
// Entrées:
//   - adresse a
//	 - adresse b
// Effet: —	Store un entier signé de 128 bits b dans a
// Sortie: Aucune
setAccByMem50:					//	void setAccByMem50 {
	ldr		x19, [x1]			//		bytes = &b
	str		x19, [x0]			//		&a = bytes
	add		x0, x0, 64			//		*a += 64
	add		x1, x1, 64			//		*b += 64
	ldr		x19, [x1]			//		bytes = &b
	str		x19, [x0]			//		&a = bytes
	ret							//	}

memAddInit:						//
	adr		x0, acc				// 	acc = *acc
	cmp		x28, 0				// 	mem[n] = *mem[0]
	adr		x1, mem0			//
	b.eq	memAddInit100		//	if(opcode == 0) setMemByAcc100
	cmp		x28, 1				//	mem[n] = *mem[1]
	adr		x1, mem1			//
	b.eq	memAddInit100		//	if(opcode == 1) setMemByAcc100
	cmp		x28, 2				//	mem[n] = *mem[2]
	adr		x1, mem2			//
	b.eq	memAddInit100		//	if(opcode == 2) setMemByAcc100

memAddInit100:					//
	bl		memAdd				//	memAdd(*acc, *mem[n])
	b 		main				//

// Sous-programme: memAdd
// Entrées:
//   - adresse a
//	 - adresse b
// Effet: — Ajoute un entier signé de 128 bits a à un b de même type
// Sortie: Aucune
memAdd:							//	void memAdd {
	SAVE						//
	ldr		x19, [x0]			//	acc1 = &a
	add		x0,	x0, 64			//	*acc += 64
	ldr		x20, [x0]			//	acc2 = &a
	ldr		x21, [x1]			//	mem1 = &b
	add		x1,	x1, 64			//	*b += 64
	ldr		x22, [x1]			//	mem2 = &b
	adds	x22, x20, x22		//	mem2 = acc2 + mem2 (update flags)
	adc		x21, x19, x21		//	mem1 = acc1 + mem1 + C
	str		x22, [x1]			//	&b = mem2
	sub		x1, x1, 64			//	*b -= 64
	str		x21, [x1]			//	&b = mem1
	RESTORE						//
	ret							// }

memSubInit:						//
	adr		x0, acc				//	acc = *acc
	adr		x1, mem0			//	mem[n] = *mem[0]
	cmp		x28, 0				//
	b.eq	memSubInit100		//	if(opcode == 0) memSubInit100
	adr		x1, mem1			//	mem[n] = *mem[1]
	cmp		x28, 1				//
	b.eq	memSubInit100		//	if(opcode == 1) memSubInit100
	adr		x1, mem2			//	mem[n] = *mem[2]
	cmp		x28, 2				//
	b.eq	memSubInit100		//	if(opcode == 2) memSubInit100

memSubInit100:

	// Rend les bits de acc négatifs
	adr		x0, acc
	bl		neg					//	neg(*acc)

	// Additionne neg(acc) et mem[n]
	bl		memAdd				//	memAdd(*acc, *mem)

	// Ajoute le complément à mem[n]
	ldr		x19, [x1]			//	part1 = &mem[n]
	add		x1, x1, 64			//	*mem[n] += 64
	ldr		x20, [x1]			//	part2 = &mem[n]
	adds	x20, x20, 1			//	part2 += 1 (update flags)
	mov		x21, 0				//	temp = 0
	adc		x19, x19, x21		//	part1 += C
	str		x20, [x1]			//	&mem[n] = part2
	sub		x1, x1, 64			//	*mem[n] -+ 64
	str		x19, [x1]			//	&mem[n] = part1

	// Remets les bits de acc positifs
	adr		x0, acc
	bl		neg					//	neg(*acc)

	b 		main

// Sous-programme: neg
// Entrées:
//   - adresse a
// Effet: — renverse tout les bits d'une adresse mémoire de 128 bits
// Sortie: a
neg:
	SAVE
	ldr		x19, [x0]			//	part1 = &a
	add		x0, x0, 64			//	*a += 64
	ldr		x20, [x0]			//	part2 = &a
	mvn		x19, x19			//	part1 = neg(part1)
	mvn		x20, x20			//	part2 = neg(part2)
	str		x20, [x0]			//	&a = part2
	sub		x0, x0, 64			//	*a -= 64
	str		x19, [x0]			//	&a = part1
	RESTORE
	ret

end:
	adr		x0, 0
    bl      exit

.section ".bss"
                .align  1
acc:          	.skip   MEMSIZE
				.align  1
mem0:          	.skip   MEMSIZE
				.align  1
mem1:          	.skip   MEMSIZE
				.align  1
mem2:          	.skip   MEMSIZE
				.align    8
nombre:       	.skip     8

.section ".rodata"
fmtOpcode:      .asciz	"%lu"
fmtOperande:    .asciz	"%lu"
fmtAcc:         .asciz	"acc:    %016lX %016lX\n"
fmtMem:         .asciz	"mem[%lu]: %016lX %016lX\n"
